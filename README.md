# Requisitos

* Java 8
* Maven 3

# Comandos

*Compilación:*

    mvn clean package

*Arranque rápido en local*

    mvn spring-boot:run

*Creación imagen docker y push al registro*

Antes de crear la imagen, adaptar en el fichero pom.xml el nombre del registro, imagen y tag. Por defecto está para docker hub.

Si se quiere cambiar el registro, se cambia el valor de 'repository'

    <repository>nexus.netexlearning.com:5001/${docker.image.name}</repository>

Construcción de la imagen:

    mvn dockerfile:build

Envío a registro

    mvn dockerfile:push

Como alternativa, en vez de usar maven, se pueden usar los comandos de docker

    docker build --build-arg VERSION=<cualquier string> -t <tag-name> .
    Ej.:
    docker build --build-arg VERSION=v1 -t nexus.netexlearning.com:5001/pruebas-trazas:latest .

y luego ya mandarla al registro con `docker push`

# Estructura

* En `src/main/java/com/netexcompany/pruebastrazas/controllers/` están el HomeController que devuelve la vista "home" y controladores dedicados para cada tipo de log que devuelven la vista "trazas"
* En `src/main/resources/templates` están las vistas creadas con thymeleaf
* En `src/main/resources` está el archivo `logback-spring.xml` con la configuración de los distintos appenders y loggers que se están usando.

# Uso de los loggers

El log por defecto se basa en el nombre del paquete donde está la clase. Uso:

    Logger logger = LoggerFactory.getLogger(<ClassName>>.class); // Se puede ver un ejemplo en TextoPlanoController

Para el uso de un logger específico por nombre de logger:

    Logger logger = LoggerFactory.getLogger("NombreDelLogger"); // Por ejemplo "GELF_LOGGER"

