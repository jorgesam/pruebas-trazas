package com.netexcompany.pruebastrazas.controllers;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/jsonlogger")
public class JsonLoggerController {

    Logger logger = LoggerFactory.getLogger("jsonLogger");

    @GetMapping("/error")
    public String error(Model model) {
        try {
            this.addLevel("error", model);
            throw new RuntimeException("Prueba traza");
        } catch (RuntimeException e) {
            logger.error("Runtime exception", e);
        }
        return "trazas";
    }

    @GetMapping("/warn")
    public String warn(Model model) {
        this.addLevel("warn", model);
        logger.warn("Mensaje en nivel warn");
        return "trazas";
    }

    @GetMapping("/info")
    public String info(Model model) {
        this.addLevel("info", model);
        logger.info("Mensaje en nivel info");
        return "trazas";
    }

    @GetMapping("/debug")
    public String debug(Model model) {
        this.addLevel("debug", model);
        logger.debug("Mensaje en nivel debug");
        return "trazas";
    }

    @GetMapping("/trace")
    public String trace(Model model) {
        this.addLevel("trace", model);
        logger.trace("Mensaje en nivel trace");
        return "trazas";
    }

    @GetMapping("/all")
    public String all(Model model) {
        this.trace(model);
        this.debug(model);
        this.info(model);
        this.warn(model);
        this.error(model);
        return "trazas";
    }

    private void addLevel(String level, Model model) {
        if (!model.containsAttribute("niveles")) {
            model.addAttribute("niveles", new ArrayList<>());
        }
        ((ArrayList) model.asMap().get("niveles")).add(level);
    }

}
