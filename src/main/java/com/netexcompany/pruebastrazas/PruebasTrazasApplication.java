package com.netexcompany.pruebastrazas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebasTrazasApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebasTrazasApplication.class, args);
    }
}
