FROM openjdk:8-jre-alpine
LABEL maintainer="jorge.saborido@netexlearning.com"

RUN mkdir /app && chmod +w /app && \
    apk add --no-cache curl bash

ARG JAR_FILE=pruebas-trazas.jar
ARG VERSION

ENV JAVA_OPTS="-XX:+UseG1GC -Dsun.net.inetaddr.ttl=60" \
    GELF_HOST=tcp:localhost \
    GELF_PORT=12201 \
    GELF_TAG=pruebastrazas \
    JAR_FILE=$JAR_FILE \
    VERSION=$VERSION

EXPOSE 8080
HEALTHCHECK --start-period=10s \
    CMD curl -f http://localhost:8080 || exit 1

COPY ["target/${JAR_FILE}", "/app/"]

CMD echo "project version: ${VERSION}" && java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app/pruebas-trazas.jar
#CMD ["sh", "-c", "echo project version: ${VERSION}", "&&", "java", "$JAVA_OPTS", "-Djava.security.egd=file:/dev/./urandom", "-jar /app/pruebas-trazas.jar"]
